create or replace type queryqueuetype is object
(
    querytext varchar2(1000),
    userId number
);

begin
    dbms_aqadm.create_queue_table(
        queue_table        => 'queryqueuetable',
        queue_payload_type => 'queryqueuetype'
    );
    dbms_aqadm.create_queue(
        queue_name  => 'queryqueue',
        queue_table => 'queryqueuetable'
    );
    dbms_aqadm.start_queue(
        queue_name  => 'queryqueue'
    );
end;
/

create or replace procedure enqueue(querytext varchar2) is
    enqueue_options     dbms_aq.enqueue_options_t;
    message_properties  dbms_aq.message_properties_t;
    message_handle      RAW(16);
    cur_session_id      number;
begin
    enqueue_options.visibility := dbms_aq.immediate;

    dbms_aq.enqueue(
         queue_name           => 'queryqueue',           
         enqueue_options      => enqueue_options,       
         message_properties   => message_properties,     
         payload              => queryqueuetype(querytext, sys_context('clientcontext', 'userId')),               
         msgid                => message_handle);
   commit;
end;
/

create or replace procedure dequeue is
    dequeue_options     dbms_aq.dequeue_options_t;
    message_properties  dbms_aq.message_properties_t;
    message_handle      RAW(16);
    l_payload           queryqueuetype;
    userId              number;
    errm varchar2(1000);
    errc number;
begin
    loop
        dequeue_options.wait := dbms_aq.no_wait;
        dbms_aq.dequeue (
            queue_name         => 'queryqueue',
            dequeue_options    => dequeue_options,
            message_properties => message_properties,
            payload            => l_payload,
            msgid              => message_handle);
        commit;
        execute immediate l_payload.querytext;
        COMMIT;
    end loop;
    EXCEPTION
    WHEN OTHERS THEN
        errc := SQLCODE;
        IF errc <> -25228 THEN
            errm := SQLERRM;
            insert into "Logs" ("LogID", "Message", "UserID", "Time") values ("LogsSeq".nextval, errm, 0, sysdate);
            commit;
        end if;
end;
/

begin
    enqueue('insert into "Logs" ("LogID", "Message", "UserID", "Time") values ("LogsSeq".nextval, ''Test'', 0, sysdate)');
--    dequeue();
--    execute immediate 'insert into "Logs" ("LogID", "Message", "UserID", "Time") values ("LogsSeq".nextval, ''Test'', 0, sysdate)';
end;
/

begin
    DBMS_SCHEDULER.CREATE_JOB(
      job_name => 'queuer',
      job_type => 'stored_procedure',
      job_action => 'dequeue',
      repeat_interval => 'FREQ=SECONDLY',
      enabled => false
    );
    DBMS_SCHEDULER.ENABLE('queuer');
    DBMS_SCHEDULER.RUN_JOB('queuer');
end;
