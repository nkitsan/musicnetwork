create or replace trigger playlists_actions
after insert or update or delete on "Playlist"
for each row
declare
    userId number := null;
    message varchar2(1000) := null;
begin
    select sys_context('clientcontext', 'userId') into userId from dual;
--    if userId is null or userId = 0 then
--        raise_application_error(-20001, 'Access denied');
--    end if;
    message := 'Playlist with id ';
    case
        when inserting then 
            message := message || to_char(:new."PlaylistID") || ' was created';
        when updating then
            message := message || to_char(:old."PlaylistID") || ' was changed';
        when deleting then
            message := message || to_char(:old."PlaylistID") || ' was deleted';
    end case;
    insert into "Logs" ("LogID", "Message", "UserID", "Time") values ("LogsSeq".nextval, message, 1, sysdate);
end;
/

create or replace trigger songs_actions
after insert or update or delete on "Song"
for each row
declare
    userId number := null;
    message varchar2(1000) := null;
begin
    select sys_context('clientcontext', 'userId') into userId from dual;
--    if userId is null or userId = 0 then
--        raise_application_error(-20001, 'Access denied');
--    end if;
    message := 'Song with id ';
    case
        when inserting then 
            message := message || to_char(:new."SongID") || ' was created';
        when updating then
            message := message || to_char(:old."SongID") || ' was changed';
        when deleting then
            message := message || to_char(:old."SongID") || ' was deleted';
    end case;
    insert into "Logs" ("LogID", "Message", "UserID", "Time") values ("LogsSeq".nextval, message, 1, sysdate);
end;
/

create or replace trigger messages_actions
after insert on "Message"
for each row
declare
    userId number := null;
    contentId number := null;
    message varchar2(1000) := null;
begin
    select sys_context('clientcontext', 'userId') into userId from dual;
    if userId is null or userId = 0 then
        raise_application_error(-20001, 'Access denied');
    end if;
    message := 'Message with id ' || to_char(:new."MessageID") || ' from ' 
    || to_char(:new."From") || ' to ' || to_char(:new."To") || ' with content ' || to_char(:new."Content") || ' was sent';
    insert into "Logs" ("LogID", "Message", "UserID", "Time") values ("LogsSeq".nextval, message, userId, sysdate);
end;
/

create or replace trigger users_actions
after insert or update or delete on "User"
for each row
declare
    userId number := null;
    message varchar2(1000) := null;
begin
    select sys_context('clientcontext', 'userId') into userId from dual;
    message := 'User with id ';
    case
        when inserting then 
            message := message || to_char(:new."UserID") || ' was created';
        when updating then
            message := message || to_char(:old."UserID") || ' was changed';
        when deleting then
            message := message || to_char(:old."UserID") || ' was deleted';
    end case;
    insert into "Logs" ("LogID", "Message", "UserID", "Time") values ("LogsSeq".nextval, message, userId, sysdate);
end;
/