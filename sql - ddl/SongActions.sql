create or replace procedure CreateSong
(
    playlistID number,
    sname varchar2,
    lyrics varchar2,
    genre varchar2,
    author varchar2,
    isOfficial number
) 
is
    songID number;
    querytext varchar2(1000);
begin
    songID := "SongSeq".nextval;
    querytext := 'insert into "Song" ("SongID", "Name", "Lyrics", "Genre", "Author", "IsOfficial")
                  values (' || to_char(songID) || ', ''' || sname || ''', ''' || lyrics || ''', ''' 
                  || genre || ''', ''' || author || ''', ' || to_char(isOfficial) || ')';
    enqueue(querytext);
    querytext := 'insert into "PlaylistSong" ("PlaylistID", "SongID") values (' || to_char(playlistID) || ', ' || to_char(songID) || ')';
    enqueue(querytext);
end;


create or replace procedure DeleteSong(songID number) is
    querytext varchar2(1000);
begin
    querytext := 'delete from "PlaylistSong" where "SongID" = ' || to_char(songID);
    enqueue(querytext);
    querytext := 'delete from "ContentSong" where "SongID" = ' || to_char(songID);
    enqueue(querytext);
    querytext := 'delete from "Song" where "SongID" = ' || to_char(songID);
    enqueue(querytext);
end;
