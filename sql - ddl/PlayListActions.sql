create or replace procedure CreatePlayList(plName varchar2, plDescription varchar2, isOfficial number, userID number, plPermission varchar2) is
    querytext varchar2(1000);
    plID number;
begin
    plID := "PlaylistSeq".nextval;
    querytext := 'insert into "Playlist" ("PlaylistID", "Name", "Description", "IsOfficial") values (' || to_char(plID) || ', ''' || plName || ''', ''' || plDescription || ''', ' || to_char(isOfficial) || ')';
    enqueue(querytext);
    querytext := 'insert into "Permission" ("PlaylistID", "UserID", "Permission") values (' || to_char(plID) || ', ' || to_char(userID) || ', ''' || plPermission || ''')';
    enqueue(querytext);
end;

create or replace procedure DeletePlayList(playlistID number) is
    querytext varchar2(1000);
begin
    querytext := 'delete from "Permission" where "plylistID" = ' || to_char(playlistID);
    enqueue(querytext);
    querytext := 'delete from "PlaylistSong" where "PlaylistID" = ' || to_char(playlistID);
    enqueue(querytext);
    querytext := 'delete from "ContentPlaylist" where "PlaylistID" = ' || to_char(playlistID);
    enqueue(querytext);
    querytext := 'delete from "Playlist" where "PlaylistID" = ' || to_char(playlistID);
    enqueue(querytext);
end;