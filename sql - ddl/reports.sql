create or replace PROCEDURE SendEmail IS
    l_workspace_id NUMBER;
BEGIN
    l_workspace_id := apex_util.find_security_group_id (p_workspace => 'MARINAMELESHKO');
    apex_util.set_security_group_id(p_security_group_id => l_workspace_id);
	APEX_MAIL.SEND(
		p_to => 'irmelen909@gmail.com', 
		p_from => 'irmelen909@gmail.com',
		p_subj => 'Daily Logs', 
		p_body => DBMS_XMLGEN.GETXML('SELECT * FROM "Logs"'));
	APEX_MAIL.PUSH_QUEUE;
END;
/

BEGIN
	DBMS_SCHEDULER.CREATE_JOB(job_name => 'daily_log',
		job_type => 'STORED_PROCEDURE',
		job_action => 'SendEmail',
		number_of_arguments => 0,
		start_date => NULL,
		end_date => NULL,
		repeat_interval => 'FREQ=HOURLY;INTERVAL=24',
		enabled => TRUE,
		auto_drop => FALSE,
		comments => 'No comments.');
END;
/

BEGIN
  DBMS_NETWORK_ACL_ADMIN.CREATE_ACL(
                                    acl         => 'apex_user.xml',
                                    description => 'access to apex email',
                                    principal   => 'SYSTEM',
                                    is_grant    => TRUE,
                                    privilege   => 'connect',
                                    end_date    =>  Null
                                    );
  DBMS_NETWORK_ACL_ADMIN.ADD_PRIVILEGE(
                                       acl       => 'apex_user.xml',
                                       principal => 'SYSTEM',
                                       is_grant  => true,
                                       privilege => 'connect'
                                       );

  DBMS_NETWORK_ACL_ADMIN.ADD_PRIVILEGE(
                                       acl       => 'apex_user.xml',
                                       principal => 'SYSTEM',
                                       is_grant  => true,
                                       privilege => 'resolve'
                                       );


  DBMS_NETWORK_ACL_ADMIN.ASSIGN_ACL(
                                    acl         => 'apex_user.xml',
                                    host        => 'smtp.gmail.com',
                                    lower_port  =>20,
                                    upper_port  =>587
                                    );
 END;

select a.host,p.*
  from dba_network_acl_privileges p
  join dba_network_acls a on a.aclid = p.aclid
 order by a.host, p.principal, p.privilege;

begin
    SendEmail();
end;
/