
CREATE TABLE Logs
(
	Time		  DATE  NULL ,
	Message		  VARCHAR2(20)  NULL ,
	LogID		  INTEGER  NOT NULL ,
	UserID		  INTEGER  NOT NULL 
);

ALTER TABLE Logs
	ADD CONSTRAINT  XPKLogs PRIMARY KEY (LogID);

CREATE TABLE Message
(
	Time		  DATE  NOT NULL ,
	MessageID	  INTEGER  NOT NULL ,
	From		  INTEGER  NOT NULL ,
	To		  INTEGER  NOT NULL ,
	Content		  INTEGER  NOT NULL 
);

ALTER TABLE Message
	ADD CONSTRAINT  XPKMessage PRIMARY KEY (MessageID);

CREATE TABLE MessageContent
(
	ContentID	  INTEGER  NOT NULL ,
	Playlists	  BOOLEAN  NULL ,
	Songs		  INTEGER  NULL ,
	Message		  VARCHAR2(2000)  NOT NULL 
);

ALTER TABLE MessageContent
	ADD CONSTRAINT  XPKMessageContent PRIMARY KEY (ContentID);

CREATE TABLE Permission
(
	Permission	  VARCHAR2(20)  NOT NULL ,
	UserID		  INTEGER  NOT NULL ,
	PlaylistID	  BOOLEAN  NOT NULL 
);

ALTER TABLE Permission
	ADD CONSTRAINT  XPKUserPlaylist PRIMARY KEY (UserID,PlaylistID);

CREATE TABLE Playlist
(
	PlaylistID	  BOOLEAN  NOT NULL ,
	Name		  VARCHAR2(20)  NOT NULL ,
	Description	  VARCHAR2(20)  NULL ,
	IsOfficial	  SMALLINT  NOT NULL ,
	Image		  BLOB  NULL 
);

ALTER TABLE Playlist
	ADD CONSTRAINT  XPKPlaylist PRIMARY KEY (PlaylistID);

CREATE TABLE PlaylistSong
(
	SongID		  INTEGER  NOT NULL ,
	PlaylistID	  BOOLEAN  NOT NULL 
);

ALTER TABLE PlaylistSong
	ADD CONSTRAINT  XPKPlaylistSong PRIMARY KEY (SongID,PlaylistID);

CREATE TABLE Song
(
	SongID		  INTEGER  NOT NULL ,
	Stream		  BLOB  NOT NULL ,
	Name		  VARCHAR2(20)  NOT NULL ,
	Lyrics		  VARCHAR2(20)  NULL ,
	Genre		  VARCHAR2(20)  NULL ,
	Author		  VARCHAR2(20)  NULL ,
	IsOfficial	  BOOLEAN  NOT NULL 
);

ALTER TABLE Song
	ADD CONSTRAINT  XPKSong PRIMARY KEY (SongID);

CREATE TABLE User
(
	UserID		  INTEGER  NOT NULL ,
	Username	  VARCHAR2(20)  NOT NULL ,
	Photo		  BLOB  NULL ,
	Password	  VARCHAR2(20)  NULL ,
	Email		  VARCHAR2(20)  NULL ,
	Role		  VARCHAR2(20)  NULL ,
	Description	  VARCHAR2(20)  NULL 
);

ALTER TABLE User
	ADD CONSTRAINT  XPKUser PRIMARY KEY (UserID);



ALTER TABLE Logs
	ADD (CONSTRAINT  LoggedUser FOREIGN KEY (UserID) REFERENCES User(UserID));



ALTER TABLE Message
	ADD (CONSTRAINT  OutcomingMessages FOREIGN KEY (From) REFERENCES User(UserID));



ALTER TABLE Message
	ADD (CONSTRAINT  IncomingMessages FOREIGN KEY (To) REFERENCES User(UserID));



ALTER TABLE Message
	ADD (CONSTRAINT  MessageContent FOREIGN KEY (Content) REFERENCES MessageContent(ContentID));



ALTER TABLE MessageContent
	ADD (CONSTRAINT  ContentList FOREIGN KEY (Playlists) REFERENCES Playlist(PlaylistID) ON DELETE SET NULL);



ALTER TABLE MessageContent
	ADD (CONSTRAINT  ContentSong FOREIGN KEY (Songs) REFERENCES Song(SongID) ON DELETE SET NULL);



ALTER TABLE Permission
	ADD (CONSTRAINT  UserPermission FOREIGN KEY (UserID) REFERENCES User(UserID));



ALTER TABLE Permission
	ADD (CONSTRAINT  PlaylistPermission FOREIGN KEY (PlaylistID) REFERENCES Playlist(PlaylistID));



ALTER TABLE PlaylistSong
	ADD (CONSTRAINT  SongOfPlaylist FOREIGN KEY (SongID) REFERENCES Song(SongID));



ALTER TABLE PlaylistSong
	ADD (CONSTRAINT  PlaylistForSong FOREIGN KEY (PlaylistID) REFERENCES Playlist(PlaylistID));