﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class DBWork
    {
        private static DBWork _instance;
        private OracleConnection _connection;

        private DBWork(string connectionString)
        {
            _connection = new OracleConnection(connectionString);
            if (_connection.State != ConnectionState.Open)
                _connection.Open();
        }
        public static DBWork Instance
        {
            get
            {
                if (_instance == null )
                    _instance = new DBWork("Data Source=XE;Password=Apz3cmcm;User ID=GURRITON");
                return _instance;
            }
        }
        private OracleCommand getCommand(string text, OracleTransaction transaction = null)
        {
            OracleCommand command = new OracleCommand();

            command.Connection = _connection;
            command.CommandType = CommandType.Text;
            command.CommandText = text;
            if (transaction != null)
                command.Transaction = transaction;

            return command;
        }

        // Users Operations
        public bool CheckUserExists(string username)
        {
            var command = getCommand(@"SELECT ""Username"" FROM  ""User"" WHERE ""Username"" = :username");

            OracleParameter authUsername = new OracleParameter()
            {
                ParameterName = "username",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.VarChar,
                Value = username
            };

            command.Parameters.Add(authUsername);

            var dbUsername = command.ExecuteScalar()?.ToString();
            return !string.IsNullOrEmpty(dbUsername);

        }
        public bool TryLogin(string username, string password)
        {
            var command = getCommand(@"SELECT ""Username"", ""Password"" FROM  ""User"" WHERE ""Username"" = :username");

            OracleParameter authUsername = new OracleParameter()
            {
                ParameterName = "username",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.VarChar,
                Value = username
            };

            command.Parameters.Add(authUsername);

            OracleDataReader reader = command.ExecuteReader();
            reader.Read();
            string dbUsername = reader.GetString(0);
            string dbPassword = reader.GetString(1);
            bool success = !string.IsNullOrEmpty(dbUsername) && password == dbPassword;
            LogOperationResult($"Somebody tried to log in by user {username}. {(success ? "Succeeded" : "Failed")}.");
            return success;
        }
        public void CreateNewUser(string username, string password, string email)
        {
            var command = getCommand(@"INSERT INTO ""User"" (""UserID"", ""Username"", ""Password"", ""Email"", ""Role"") VALUES (""UserSeq"".NEXTVAL, :username, :password, :email, 'USER')");

            OracleParameter Username = new OracleParameter()
            {
                ParameterName = "username",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.VarChar,
                Value = username
            };

            OracleParameter Password = new OracleParameter()
            {
                ParameterName = "password",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.VarChar,
                Value = password
            };

            OracleParameter Email = new OracleParameter()
            {
                ParameterName = "email",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.VarChar,
                Value = email
            };

            command.Parameters.Add(Username);
            command.Parameters.Add(Password);
            command.Parameters.Add(Email);

            command.ExecuteNonQuery();
            LogOperationResult($"Created new user {username}.");
        }
        public (int ID, string Username, string Password, string Email, string Role, string Description)? GetUser(string username)
        {
            var command = getCommand(@"SELECT ""UserID"", ""Username"", ""Password"", ""Email"", ""Role"", ""Description"" FROM  ""User"" WHERE ""Username"" = :username");

            OracleParameter Username = new OracleParameter()
            {
                ParameterName = "username",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.VarChar,
                Value = username
            };

            command.Parameters.Add(Username);

            (int ID, string Username, string Password, string Email, string Role, string Description)? user = null;
            var reader = command.ExecuteReader();
            if (reader.Read())
            {
                user = (reader.GetInt32(0), reader.GetValue(1) == DBNull.Value ? null : reader.GetString(1), reader.GetValue(2) == DBNull.Value ? null : reader.GetString(2), reader.GetValue(3) == DBNull.Value ? null : reader.GetString(3), reader.GetValue(4) == DBNull.Value ? null : reader.GetString(4), reader.GetValue(5) == DBNull.Value ? null : reader.GetString(5));
            }

            return user;
        }
        public List<(int ID, string Username, string Email, string Role, string Description)> GetUsersByPrefix(string prefix)
        {
            var command = getCommand(@"SELECT ""UserID"", ""Username"", ""Email"", ""Role"", ""Description"" FROM  ""User"" WHERE ""Username"" LIKE :prefix");

            OracleParameter Prefix = new OracleParameter()
            {
                ParameterName = "prefix",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.VarChar,
                Value = prefix + "%"
            };

            command.Parameters.Add(Prefix);

            List<(int ID, string Username, string Email, string Role, string Description)> users = new List<(int ID, string Username, string Email, string Role, string Description)>();

            var reader = command.ExecuteReader();
            while (reader.Read())
            {
                users.Add((reader.GetInt32(0), reader.GetValue(1) == DBNull.Value ? null : reader.GetString(1), reader.GetValue(2) == DBNull.Value ? null : reader.GetString(2), reader.GetValue(3) == DBNull.Value ? null : reader.GetString(3), reader.GetValue(4) == DBNull.Value ? null : reader.GetString(4)));
            }

            return users;
        }
        public byte[] GetUserpic(int userID)
        {
            var command = getCommand(@"SELECT ""Photo"" FROM  ""User"" WHERE ""UserID"" = :userID");

            OracleParameter UserID = new OracleParameter()
            {
                ParameterName = "userID",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.Int32,
                Value = userID
            };

            command.Parameters.Add(UserID);

            var str = command.ExecuteScalar();
            if (str != DBNull.Value)
                return (byte[])str;
            return null;
        }
        public void EditUser(int userID, string username = null, byte[] photo = null, string password = null, string email = null, string role = null,
            string description = null)
        {
            var user = GetUser(GetUsernameByID(userID));
            if (username == null)
                username = user.Value.Username;
            if (photo == null)
                photo = GetUserpic(userID);
            if (password == null)
                password = user.Value.Password;
            if (email == null)
                email = user.Value.Email;
            if (role == null)
                role = user.Value.Role;
            if (description == null)
                description = user.Value.Description;

            var command = getCommand(@"UPDATE ""User"" SET
                                       ""Username"" = :username, ""Photo"" = :photo, ""Password"" = :password, ""Email"" = :email, ""Role"" = :role, ""Description"" = :description
                                       WHERE ""UserID"" = :userID");

            OracleParameter UserID = new OracleParameter()
            {
                ParameterName = "userID",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.Int32,
                Value = userID
            };
            OracleParameter Username = new OracleParameter()
            {
                ParameterName = "username",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.VarChar,
                Value = username
            };
            OracleParameter Photo = new OracleParameter()
            {
                ParameterName = "photo",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.Blob,
                Value = photo
            };
            OracleParameter Password = new OracleParameter()
            {
                ParameterName = "password",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.VarChar,
                Value = password
            };
            OracleParameter Email = new OracleParameter()
            {
                ParameterName = "email",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.VarChar,
                Value = email
            };
            OracleParameter Role = new OracleParameter()
            {
                ParameterName = "role",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.VarChar,
                Value = role
            };
            OracleParameter Description = new OracleParameter()
            {
                ParameterName = "description",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.VarChar,
                Value = description
            };

            command.Parameters.Add(UserID);
            command.Parameters.Add(Username);
            command.Parameters.Add(Photo);
            command.Parameters.Add(Password);
            command.Parameters.Add(Email);
            command.Parameters.Add(Role);
            command.Parameters.Add(Description);

            command.ExecuteNonQuery();

            LogOperationResult($"User {userID} has been edited", userID);
        }
        public string GetUsernameByID(int userID)
        {
            var command = getCommand(@"SELECT ""Username"" FROM  ""User"" WHERE ""UserID"" = :userID");

            OracleParameter UserID = new OracleParameter()
            {
                ParameterName = "userID",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.Int32,
                Value = userID
            };

            command.Parameters.Add(UserID);

            var value = command.ExecuteScalar();
            if (value != DBNull.Value)
                return value.ToString();
            return null;
        }

        // Logs Operations
        public void LogOperationResult(string message, int userID = 0)
        {
            var command = getCommand(@"INSERT INTO ""Logs"" (""LogID"", ""Message"", ""UserID"", ""Time"") VALUES (""LogsSeq"".NEXTVAL, :message, :userID, SYSDATE)");

            OracleParameter Message = new OracleParameter()
            {
                ParameterName = "message",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.VarChar,
                Value = message
            };

            OracleParameter UserID = new OracleParameter()
            {
                ParameterName = "userID",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.Int32,
                Value = userID
            };

            command.Parameters.Add(Message);
            command.Parameters.Add(UserID);

            command.ExecuteNonQuery();
        }
        public List<(int LogID, string Message, int UserID, DateTime Time)> GetLogs()
        {
            var command = getCommand(@"SELECT ""LogID"", ""Message"", ""UserID"", ""Time"" FROM  ""Logs""");

            List<(int LogID, string Message, int UserID, DateTime Time)> result = new List<(int LogID, string Message, int UserID, DateTime Time)>();
            OracleDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                result.Add((reader.GetInt32(0), reader.GetString(1), reader.GetInt32(2), reader.GetDateTime(3)));
            }
            return result;
        }

        // Playlists Operations
        public void CreatePlaylist(int userID, string name, string permission, byte[] image = null, string description = null, bool isOfficial = false)
        {
            OracleTransaction transaction = _connection.BeginTransaction();
            int newID = 0;
            try
            {
                var command = getCommand(@"SELECT ""PlaylistSeq"".NEXTVAL FROM DUAL", transaction);
                newID = (int)(decimal)command.ExecuteScalar();

                command = getCommand(@"INSERT INTO ""Playlist"" (""PlaylistID"", ""Name"", ""Description"", ""IsOfficial"", ""Image"") 
                                       VALUES (:newID, :name, :description, :isOfficial, :image)", transaction);

                OracleParameter NewID = new OracleParameter()
                {
                    ParameterName = "newID",
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.Int32,
                    Value = newID
                };
                OracleParameter Name = new OracleParameter()
                {
                    ParameterName = "name",
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    Value = name
                };
                OracleParameter Description = new OracleParameter()
                {
                    ParameterName = "description",
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    Value = description
                };
                OracleParameter IsOfficial = new OracleParameter()
                {
                    ParameterName = "isOfficial",
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.Int32,
                    Value = isOfficial ? 1 : 0
                };
                OracleParameter Image = new OracleParameter()
                {
                    ParameterName = "image",
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.Blob,
                    Value = image
                };

                command.Parameters.Add(NewID);
                command.Parameters.Add(Name);
                command.Parameters.Add(Image);
                command.Parameters.Add(Description);
                command.Parameters.Add(IsOfficial);

                command.ExecuteNonQuery();

                command = getCommand(@"INSERT INTO ""Permission"" (""PlaylistID"", ""UserID"", ""Permission"") VALUES (:playlistID, :userID, :permission)", transaction);

                OracleParameter UserID = new OracleParameter()
                {
                    ParameterName = "userID",
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.Int32,
                    Value = userID
                };
                OracleParameter PlaylistID = new OracleParameter()
                {
                    ParameterName = "playlistID",
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.Int32,
                    Value = newID
                };
                OracleParameter Permission = new OracleParameter()
                {
                    ParameterName = "permission",
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.VarChar,
                    Value = permission
                };

                command.Parameters.Add(UserID);
                command.Parameters.Add(PlaylistID);
                command.Parameters.Add(Permission);

                command.ExecuteNonQuery();
                transaction.Commit();
            }
            catch
            {
                transaction.Rollback();
            }

            LogOperationResult($"Created new playlist {newID}.", userID);
        }
        public string CheckPlaylistAccess(int userID, int playlistID)
        {
            var command = getCommand(@"SELECT ""Permission"" FROM ""Permission"" WHERE ""UserID"" = :userID AND ""PlaylistID"" = :playlistID");

            OracleParameter UserID = new OracleParameter()
            {
                ParameterName = "userID",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.Int32,
                Value = userID
            };

            OracleParameter PlaylistID = new OracleParameter()
            {
                ParameterName = "playlistID",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.Int32,
                Value = playlistID
            };

            command.Parameters.Add(UserID);
            command.Parameters.Add(PlaylistID);

            return command.ExecuteScalar()?.ToString();
        }
        public void DeletePlaylist(int playlistID, int userID)
        {
            OracleTransaction transaction = _connection.BeginTransaction();
            int newID = 0;
            try
            {
                OracleParameter PlaylistID = new OracleParameter()
            {
                ParameterName = "playlistID",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.Int32,
                Value = playlistID
            };

            var command = getCommand(@"DELETE FROM ""Permission"" WHERE ""PlaylistID"" = :playlistID", transaction);
            command.Parameters.Add(PlaylistID);
            command.ExecuteNonQuery();

            command = getCommand(@"DELETE FROM ""PlaylistSong"" WHERE ""PlaylistID"" = :playlistID", transaction);
            PlaylistID = new OracleParameter()
            {
                ParameterName = "playlistID",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.Int32,
                Value = playlistID
            };
            command.Parameters.Add(PlaylistID);
            command.ExecuteNonQuery();

            command = getCommand(@"DELETE FROM ""ContentPlaylist"" WHERE ""PlaylistID"" = :playlistID", transaction);
            PlaylistID = new OracleParameter()
            {
                ParameterName = "playlistID",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.Int32,
                Value = playlistID
            };
            command.Parameters.Add(PlaylistID);
            command.ExecuteNonQuery();

            command = getCommand(@"DELETE FROM ""Playlist"" WHERE ""PlaylistID"" = :playlistID", transaction);
            PlaylistID = new OracleParameter()
            {
                ParameterName = "playlistID",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.Int32,
                Value = playlistID
            };
                command.Parameters.Add(PlaylistID);
                command.ExecuteNonQuery();
                transaction.Commit();
            }
            catch
            {
                transaction.Rollback();
            }
            LogOperationResult($"User {userID} deleted playlist {playlistID}.", userID);
        }
        public byte[] GetPlaylistCover(int playlistID)
        {
            var command = getCommand(@"SELECT ""Image"" FROM  ""Playlist"" WHERE ""PlaylistID"" = :playlistID");

            OracleParameter PlaylistID = new OracleParameter()
            {
                ParameterName = "playlistID",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.Int32,
                Value = playlistID
            };

            command.Parameters.Add(PlaylistID);

            var value = command.ExecuteScalar();
            if (value != DBNull.Value)
                return (byte[])value;
            return null;
        }
        public List<(int ID, string Name, string Description, bool IsOfficial)> GetPlaylistsByPrefix(string prefix)
        {
            List<(int ID, string Name, string Description, bool IsOfficial)> playlists = new List<(int ID, string Name, string Description, bool IsOfficial)>();

            var command = getCommand(@"SELECT ""PlaylistID"", ""Name"", ""Description"", ""IsOfficial"" FROM  ""Playlist"" 
                                       WHERE ""Name"" LIKE :prefix");

            OracleParameter Prefix = new OracleParameter()
            {
                ParameterName = "prefix",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.VarChar,
                Value = prefix + "%"
            };

            command.Parameters.Add(Prefix);

            OracleDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                playlists.Add((reader.GetInt32(0), reader.GetString(1), reader.GetValue(2) == DBNull.Value ? null : reader.GetString(2), reader.GetInt32(3) != 0));
            }

            return playlists;
        }
        public (int ID, string Name, string Description, bool IsOfficial)? GetPlaylistByID(int playlistID)
        {
            var command = getCommand(@"SELECT ""PlaylistID"", ""Name"", ""Description"", ""IsOfficial"" FROM  ""Playlist"" 
                                       WHERE ""PlaylistID"" = :playlistID");

            OracleParameter PlaylistID = new OracleParameter()
            {
                ParameterName = "playlistID",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.Int32,
                Value = playlistID
            };

            command.Parameters.Add(PlaylistID);

            OracleDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                return (reader.GetInt32(0), reader.GetString(1), reader.GetValue(2) == DBNull.Value ? null : reader.GetString(2), reader.GetInt32(3) != 0);
            }

            return null;
        }
        public List<(int ID, string Name, string Description, bool IsOfficial)> GetPlaylistsByUser(int userID)
        {
            List<(int ID, string Name, string Description, bool IsOfficial)> playlists = new List<(int ID, string Name, string Description, bool IsOfficial)>();

            var command = getCommand(@"SELECT ""Playlist"".""PlaylistID"", ""Name"", ""Description"", ""IsOfficial"" FROM  ""Playlist"" 
                                       JOIN ""Permission"" ON ""Playlist"".""PlaylistID""=""Permission"".""PlaylistID"" WHERE ""Permission"".""UserID"" = :userID");

            OracleParameter UserID = new OracleParameter()
            {
                ParameterName = "userID",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.Int32,
                Value = userID
            };

            command.Parameters.Add(UserID);

            OracleDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                playlists.Add((reader.GetInt32(0), reader.GetString(1), reader.GetValue(2) == DBNull.Value ? null : reader.GetString(2), reader.GetInt32(3) != 0));
            }

            return playlists;
        }

        // Songs Operations
        public void CreateSong(int playlistID, string name, byte[] stream, string lyrics = null, string genre = null, string author = null, bool isOfficial = false, int userID = 0)
        {
            OracleTransaction transaction = _connection.BeginTransaction();
            int newID = 0;
            try
            {
                var command = getCommand(@"SELECT ""SongSeq"".NEXTVAL FROM DUAL", transaction);
                newID = (int)(decimal)command.ExecuteScalar();

            command = getCommand(@"INSERT INTO ""Song"" (""SongID"", ""Name"", ""Lyrics"", ""Genre"", ""Author"", ""IsOfficial"", ""Stream"") 
                                       VALUES (:newID, :name, :lyrics, :genre, :author, :isOfficial, :stream)", transaction);

            OracleParameter NewID = new OracleParameter()
            {
                ParameterName = "newID",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.Int32,
                Value = newID
            };
            OracleParameter Name = new OracleParameter()
            {
                ParameterName = "name",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.VarChar,
                Value = name
            };
            OracleParameter Lyrics = new OracleParameter()
            {
                ParameterName = "lyrics",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.VarChar,
                Value = lyrics
            };
            OracleParameter Genre = new OracleParameter()
            {
                ParameterName = "genre",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.VarChar,
                Value = genre
            };
            OracleParameter Author = new OracleParameter()
            {
                ParameterName = "author",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.VarChar,
                Value = author
            };
            OracleParameter IsOfficial = new OracleParameter()
            {
                ParameterName = "isOfficial",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.Int32,
                Value = isOfficial ? 1 : 0
            };
            OracleParameter Stream = new OracleParameter()
            {
                ParameterName = "stream",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.Blob,
                Value = stream
            };

            command.Parameters.Add(NewID);
            command.Parameters.Add(Name);
            command.Parameters.Add(Stream);
            command.Parameters.Add(Lyrics);
            command.Parameters.Add(Genre);
            command.Parameters.Add(Author);
            command.Parameters.Add(IsOfficial);

            command.ExecuteNonQuery();

            command = getCommand(@"INSERT INTO ""PlaylistSong"" (""PlaylistID"", ""SongID"") VALUES (:playlistID, :songID)", transaction);

            OracleParameter SongID = new OracleParameter()
            {
                ParameterName = "songID",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.Int32,
                Value = newID
            };
            OracleParameter PlaylistID = new OracleParameter()
            {
                ParameterName = "playlistID",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.Int32,
                Value = playlistID
            };

            command.Parameters.Add(SongID);
            command.Parameters.Add(PlaylistID);

            command.ExecuteNonQuery();
            transaction.Commit();
            }
            catch
            {
                transaction.Rollback();
            }
    LogOperationResult($"Created new song {newID} in playlist {playlistID}.", userID);
        }
        public List<(int ID, string Name, string Lyrics, string Genre, string Author, bool IsOfficial)>
            GetSongsByPrefix(string prefix)
        {
            List<(int ID, string Name, string Lyrics, string Genre, string Author, bool IsOfficial)> songs = new List<(int ID, string Name, string Lyrics, string Genre, string Author, bool IsOfficial)>();

            var command = getCommand(@"SELECT ""SongID"", ""Name"", ""Lyrics"", ""Genre"", ""Author"", ""IsOfficial"" FROM  ""Song"" 
                                       WHERE ""Name"" LIKE :prefix");

            OracleParameter Prefix = new OracleParameter()
            {
                ParameterName = "prefix",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.VarChar,
                Value = prefix + "%"
            };

            command.Parameters.Add(Prefix);

            OracleDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                songs.Add((reader.GetInt32(0), reader.GetString(1), reader.GetValue(2) == DBNull.Value ? null : reader.GetString(2), reader.GetValue(3) == DBNull.Value ? null : reader.GetString(3), reader.GetValue(4) == DBNull.Value ? null : reader.GetString(4), reader.GetInt32(5) != 0));
            }

            return songs;
        }
        public byte[] GetSongStream(int songID)
        {
            var command = getCommand(@"SELECT ""Stream"" FROM  ""Song"" WHERE ""SongID"" = :songID");

            OracleParameter SongID = new OracleParameter()
            {
                ParameterName = "songID",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.Int32,
                Value = songID
            };

            command.Parameters.Add(SongID);

            var value = command.ExecuteScalar();
            if (value != DBNull.Value)
                return (byte[])value;
            return null;
        }
        public void DeleteSong(int songID, int userID)
        {
            OracleParameter SongID = new OracleParameter()
            {
                ParameterName = "songID",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.Int32,
                Value = songID
            };

            var command = getCommand(@"DELETE FROM ""PlaylistSong"" WHERE ""SongID"" = :songID");
            command.Parameters.Add(SongID);
            command.ExecuteNonQuery();

            command = getCommand(@"DELETE FROM ""ContentSong"" WHERE ""SongID"" = :songID");
            command.Parameters.Add(SongID);
            command.ExecuteNonQuery();

            command = getCommand(@"DELETE FROM ""Song"" WHERE ""SongID"" = :songID");
            command.Parameters.Add(SongID);
            command.ExecuteNonQuery();

            LogOperationResult($"User {userID} deleted song {songID}", userID);
        }
        public List<(int ID, string Name, string Lyrics, string Genre, string Author, bool IsOfficial)>
            GetSongsByPlaylist(int playlistID)
        {
            List<(int ID, string Name, string Lyrics, string Genre, string Author, bool IsOfficial)> songs = new List<(int ID, string Name, string Lyrics, string Genre, string Author, bool IsOfficial)>();

            var command = getCommand(@"SELECT ""Song"".""SongID"", ""Name"", ""Lyrics"", ""Genre"", ""Author"", ""IsOfficial"" FROM  ""Song"" 
                                       JOIN ""PlaylistSong"" ON ""Song"".""SongID""=""PlaylistSong"".""SongID"" WHERE ""PlaylistSong"".""PlaylistID"" = :playlistID");

            OracleParameter PlaylistID = new OracleParameter()
            {
                ParameterName = "playlistID",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.Int32,
                Value = playlistID
            };

            command.Parameters.Add(PlaylistID);

            OracleDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                songs.Add((reader.GetInt32(0), reader.GetString(1), reader.GetValue(2) == DBNull.Value ? null : reader.GetString(2), reader.GetValue(3) == DBNull.Value ? null : reader.GetString(3), reader.GetValue(4) == DBNull.Value ? null : reader.GetString(4), reader.GetInt32(5) != 0));
            }

            return songs;
        }
        public (int ID, string Name, string Lyrics, string Genre, string Author, bool IsOfficial)? GetSongByID(int songID)
        {
            var command = getCommand(@"SELECT ""SongID"", ""Name"", ""Lyrics"", ""Genre"", ""Author"", ""IsOfficial"" FROM  ""Song"" 
                                       WHERE ""SongID"" = :songID");

            OracleParameter SongID = new OracleParameter()
            {
                ParameterName = "songID",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.Int32,
                Value = songID
            };

            command.Parameters.Add(SongID);

            OracleDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                return (reader.GetInt32(0), reader.GetString(1), reader.GetValue(2) == DBNull.Value ? null : reader.GetString(2), reader.GetValue(3) == DBNull.Value ? null : reader.GetString(3), reader.GetValue(4) == DBNull.Value ? null : reader.GetString(4), reader.GetInt32(5) != 0);
            }

            return null;
        }

        //Messages Operations
        public List<(int MessageID, DateTime Time, int FromID, int ToID, int ContentID, string Message)> GetMessages(int userID)
        {
            List<(int MessageID, DateTime Time, int FromID, int ToID, int ContentID, string Message)> messages = new List<(int MessageID, DateTime Time, int FromID, int ToID, int ContentID, string Message)>();

            var command = getCommand(@"SELECT ""MessageID"", ""Time"", ""From"", ""To"", ""Content"", ""MessageContent"".""Message"" FROM  ""Message"" 
                                       JOIN ""MessageContent"" ON ""Message"".""Content"" = ""MessageContent"".""ContentID""
                                       WHERE ""To"" = :userID");

            OracleParameter UserID = new OracleParameter()
            {
                ParameterName = "userID",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.Int32,
                Value = userID
            };

            command.Parameters.Add(UserID);

            OracleDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                messages.Add((reader.GetInt32(0), reader.GetDateTime(1), reader.GetInt32(2), reader.GetInt32(3), reader.GetInt32(4), reader.GetString(5)));
            }

            return messages;
        }
        public List<(int ID, string Name, string Description, bool IsOfficial)> GetPlaylistsByContent(int contentID)
        {
            List<(int ID, string Name, string Description, bool IsOfficial)> playlists = new List<(int ID, string Name, string Description, bool IsOfficial)>();

            var command = getCommand(@"SELECT ""Playlist"".""PlaylistID"", ""Name"", ""Description"", ""IsOfficial"" FROM  ""Playlist"" 
                                       JOIN ""ContentPlaylist"" ON ""Playlist"".""PlaylistID""=""ContentPlaylist"".""PlaylistID"" WHERE ""ContentPlaylist"".""ContentID"" = :contentID");

            OracleParameter ContentID = new OracleParameter()
            {
                ParameterName = "contentID",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.Int32,
                Value = contentID
            };

            command.Parameters.Add(ContentID);

            OracleDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                playlists.Add((reader.GetInt32(0), reader.GetString(1), reader.GetValue(2) == DBNull.Value ? null : reader.GetString(2), reader.GetInt32(3) != 0));
            }

            return playlists;
        }
        public List<(int ID, string Name, string Lyrics, string Genre, string Author, bool IsOfficial)>
            GetSongsByContent(int contentID)
        {
            List<(int ID, string Name, string Lyrics, string Genre, string Author, bool IsOfficial)> songs = new List<(int ID, string Name, string Lyrics, string Genre, string Author, bool IsOfficial)>();

            var command = getCommand(@"SELECT ""Song"".""SongID"", ""Name"", ""Lyrics"", ""Genre"", ""Author"", ""IsOfficial"" FROM  ""Song"" 
                                       JOIN ""ContentSong"" ON ""Song"".""SongID""=""ContentSong"".""SongID"" WHERE ""ContentSong"".""ContentID"" = :contentID");

            OracleParameter ContentID = new OracleParameter()
            {
                ParameterName = "contentID",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.Int32,
                Value = contentID
            };

            command.Parameters.Add(ContentID);

            OracleDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                songs.Add((reader.GetInt32(0), reader.GetString(1), reader.GetValue(1) == DBNull.Value ? null : reader.GetString(2), reader.GetValue(1) == DBNull.Value ? null : reader.GetString(3), reader.GetValue(1) == DBNull.Value ? null : reader.GetString(4), reader.GetInt32(5) != 0));
            }

            return songs;
        }
        public void CreateMessage(int fromId, int toID, List<int> songs, List<int> playlists, string message)
        {
            int contentID = (int)(decimal)getCommand(@"SELECT ""ContentSeq"".NEXTVAL FROM DUAL").ExecuteScalar();
            int messageID = (int)(decimal)getCommand(@"SELECT ""MessageSeq"".NEXTVAL FROM DUAL").ExecuteScalar();
            var command = getCommand(@"INSERT INTO ""MessageContent"" (""ContentID"", ""Message"") VALUES (:contentID, :message)");

            OracleParameter ContentID = new OracleParameter()
            {
                ParameterName = "contentID",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.Int32,
                Value = contentID
            };
            OracleParameter Message = new OracleParameter()
            {
                ParameterName = "message",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.VarChar,
                Value = message
            };

            command.Parameters.Add(ContentID);
            command.Parameters.Add(Message);
            command.ExecuteNonQuery();

            foreach (var song in songs)
            {
                command = getCommand(@"INSERT INTO ""ContentSong"" (""ContentID"", ""SongID"") VALUES (:contentID, :songID)");

                ContentID = new OracleParameter
                {
                    ParameterName = "contentID",
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.Int32,
                    Value = contentID
                };
                OracleParameter SongID = new OracleParameter
                {
                    ParameterName = "songID",
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.Int32,
                    Value = GetSongByID(song).Value.ID
                };

                command.Parameters.Add(ContentID);
                command.Parameters.Add(SongID);
                command.ExecuteNonQuery();
            }
            foreach (var playlist in playlists)
            {
                command = getCommand(@"INSERT INTO ""ContentPlaylist"" (""ContentID"", ""PlaylistID"") VALUES (:contentID, :playlistID)");

                ContentID = new OracleParameter
                {
                    ParameterName = "contentID",
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.Int32,
                    Value = contentID
                };
                OracleParameter PlaylistID = new OracleParameter
                {
                    ParameterName = "playlistID",
                    Direction = ParameterDirection.Input,
                    OracleType = OracleType.Int32,
                    Value = GetPlaylistByID(playlist).Value.ID
                };

                command.Parameters.Add(ContentID);
                command.Parameters.Add(PlaylistID);
                command.ExecuteNonQuery();
            }

            command = getCommand(@"INSERT INTO ""Message"" (""MessageID"", ""Time"", ""From"", ""To"", ""Content"") VALUES (:messageID, SYSDATE, :fromID, :toID, :contentID)");

            OracleParameter MessageID = new OracleParameter()
            {
                ParameterName = "messageID",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.Int32,
                Value = messageID
            };
            OracleParameter FromID = new OracleParameter()
            {
                ParameterName = "fromID",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.Int32,
                Value = fromId
            };
            OracleParameter ToID = new OracleParameter()
            {
                ParameterName = "toID",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.Int32,
                Value = toID
            };
            ContentID = new OracleParameter()
            {
                ParameterName = "contentID",
                Direction = ParameterDirection.Input,
                OracleType = OracleType.Int32,
                Value = contentID
            };

            command.Parameters.Add(MessageID);
            command.Parameters.Add(FromID);
            command.Parameters.Add(ToID);
            command.Parameters.Add(ContentID);

            command.ExecuteNonQuery();

            LogOperationResult($"User {fromId} sent a message to user {toID} with content {contentID}", fromId);
        }
    }
}
