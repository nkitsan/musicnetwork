﻿using System;
using System.Collections.Generic;
using System.IO;
using static System.Console;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            var db = DBWork.Instance;
            //WriteLine("Users test");
            //db.CreateNewUser("Vasya", "Abababa", "sadasd@ua.ua");
            //WriteLine(db.CheckUserExists("Vasya"));
            //WriteLine(db.CheckUserExists("Petya"));
            //db.CreateNewUser("Petya", "Abababa", "sadasd@ua.ua");
            //WriteLine(db.TryLogin("Vasya", "aaa"));
            //WriteLine(db.TryLogin("Vasya", "Abababa"));
            //WriteLine(db.GetUser("Vasya"));
            //foreach (var user in db.GetUsersByPrefix("Va"))
            //    WriteLine(user);
            //foreach (var user in db.GetUsersByPrefix("Petya"))
            //    WriteLine(user);
            //foreach (var user in db.GetUsersByPrefix("Pi"))
            //    WriteLine(user);
            int VasyaID = db.GetUser("Vasya").Value.ID;
            //var data = File.ReadAllBytes("enot.jpg");
            //db.EditUser(VasyaID, photo: data);
            //data = db.GetUserpic(VasyaID);
            //File.WriteAllBytes("enot2.jpg", data);
            //WriteLine(db.GetUsernameByID(0));
            //WriteLine("Playlists tests");
            db.CreatePlaylist(VasyaID, "TopCheng", "EDIT", image: File.ReadAllBytes("enot2.jpg"), isOfficial: true);
            db.CreatePlaylist(VasyaID, "TopChug", "EDIT", image: File.ReadAllBytes("enot.jpg"), isOfficial: true);
            //foreach (var pl in db.GetPlaylistsByPrefix("Top"))
            //    WriteLine(pl);
            //foreach (var pl in db.GetPlaylistsByUser(VasyaID))
            //    WriteLine(pl);
            //db.DeletePlaylist(25, VasyaID);
            //db.CreateMessage(VasyaID, VasyaID + 1, new List<int> { 1 }, new List<int> { 24 }, "hello");
            foreach (var log in db.GetLogs())
                WriteLine(log);
            ReadKey();
        }
    }
}
