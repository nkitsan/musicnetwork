﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Controllers
{
    [Produces("application/json")]
    [Route("api/songs")]
    public class SongsController : Controller
    {
        [Authorize]
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            return File(DBWork.Instance.GetSongStream(id), "music", $"song{id}.mp3");
        }

        [Authorize]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var user = DBWork.Instance.GetUser(User.Identity.Name).Value;
            if (user.Role == "ADMIN")
                DBWork.Instance.DeleteSong(id, user.ID);
            return Ok();
        }
    }
}