﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Backend.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Controllers
{
    [Produces("application/json")]
    [Route("api/playlists")]
    public class PlaylistsController : Controller
    {
        [Authorize]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var user = DBWork.Instance.GetUser(User.Identity.Name);
            if (user?.Role == "ADMIN" || !string.IsNullOrEmpty(DBWork.Instance.CheckPlaylistAccess(user.Value.ID, id)))
                DBWork.Instance.DeletePlaylist(id, user.Value.ID);
            return Ok();
        }

        [Authorize]
        [HttpGet("{id}/songs")]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(DBWork.Instance.GetSongsByPlaylist(id).Select(x => new Sound { Author = x.Author, Name = x.Name, SoundId = x.ID }).ToList());
        }

        [Authorize]
        [HttpPost("{id}/songs")]
        public async Task<IActionResult> Post(int id, IFormFile file)
        {
            string songName = Request.Form.FirstOrDefault(p => p.Key == "name").Value;
            string songAuthor = Request.Form.FirstOrDefault(p => p.Key == "author").Value;
            MemoryStream ms = new MemoryStream();
            file?.CopyTo(ms);
            DBWork.Instance.CreateSong(id, songName, ms.ToArray(), author: songAuthor, userID: DBWork.Instance.GetUser(User.Identity.Name).Value.ID);
            return Ok();
        }

        [Authorize]
        [HttpGet("{id}/photo")]
        public async Task<IActionResult> GetCover(int id)
        {
            return File(DBWork.Instance.GetPlaylistCover(id), "img", $"cover{id}.jpg");
        }
    }
}