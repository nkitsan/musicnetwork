﻿using Backend.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace Backend.Controllers
{
    public class HomeController : Controller
    {
        [Authorize]
        public IActionResult Index()
        {
            var username = User.Identity.Name;
            int id = DBWork.Instance.GetUser(username).Value.ID;
            var playlists = DBWork.Instance.GetPlaylistsByUser(id).Select(x => new SoundCollection { IsOfficial = x.IsOfficial, Name = x.Name, SoundCollectionId = x.ID }).ToList();
            return View(playlists);
        }

        [Authorize]
        public IActionResult Songs(int playlistId)
        {
            //var username = User.Identity.Name;
            ViewBag.playListId = playlistId;
            //int id = DBWork.Instance.GetUser(username).Value.ID;
            var songs = DBWork.Instance.GetSongsByPlaylist(playlistId).Select(x => new Sound { Author = x.Author, Name = x.Name, SoundId = x.ID }).ToList();
            return View(songs);
        }

        [Authorize]
        public IActionResult Logs()
        {
            var username = User.Identity.Name;
            List<Log> logs = new List<Log>();
            if (DBWork.Instance.GetUser(username)?.Role == "ADMIN")
                logs = DBWork.Instance.GetLogs().Select(x => new Log { Message = x.Message, UserId = x.UserID, Time = x.Time }).ToList();
            return View(logs);
        }
    }
}