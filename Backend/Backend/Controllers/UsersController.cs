﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Backend.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Controllers
{
    [Produces("application/json")]
    [Route("api/users")]
    public class UsersController : Controller
    {
        [Authorize]
        [HttpGet]
        public async Task<IActionResult> GetIdentity()
        {
            return Ok(User.Identity.Name);
        }

        [Authorize]
        [HttpGet("{nickname}/playlists")]
        public async Task<IActionResult> Get(string nickname)
        {
            int id = DBWork.Instance.GetUser(nickname).Value.ID;
            return Ok(DBWork.Instance.GetPlaylistsByUser(id).Select(x => new SoundCollection { IsOfficial = x.IsOfficial, Name = x.Name, SoundCollectionId = x.ID }));
        }

        [Authorize]
        [HttpPost("{nickname}/playlists")]
        public async Task<IActionResult> Post(string nickname, IFormFile file)
        {
            int id = DBWork.Instance.GetUser(nickname).Value.ID;
            string name = Request.Form.FirstOrDefault(p => p.Key == "name").Value;
            bool isOfficial = bool.Parse(Request.Form.FirstOrDefault(p => p.Key == "is_official").Value);
            DBWork.Instance.CreatePlaylist(id, name, "EDIT", isOfficial: isOfficial);
            return Ok();
        }

        [Authorize]
        [HttpPost("{nickname}/photo")]
        public async Task<IActionResult> PostUserpic(string nickname,IFormFile file)
        {
            int id = DBWork.Instance.GetUser(nickname).Value.ID;
            MemoryStream ms = new MemoryStream();
            file.CopyTo(ms);
            DBWork.Instance.EditUser(id, photo: ms.ToArray());
            return Ok();
        }

        [Authorize]
        [HttpGet("{nickname}/photo")]
        public async Task<IActionResult> GetUserpic(string nickname)
        {
            int id = DBWork.Instance.GetUser(nickname).Value.ID;
            return File(DBWork.Instance.GetUserpic(id), "img", $"user{id}.jpg");
        }

        [Authorize]
        [HttpGet("{nickname}/messages")]
        public async Task<IActionResult> GetMessages(string nickname)
        {
            int id = DBWork.Instance.GetUser(nickname).Value.ID;
            List<Message> messages = DBWork.Instance.GetMessages(id).Select(x =>
            {
                var song = DBWork.Instance.GetSongsByContent(x.ContentID).First();
                return new Message
                {
                    From = DBWork.Instance.GetUsernameByID(x.FromID),
                    To = DBWork.Instance.GetUsernameByID(x.ToID),
                    Time = x.Time.ToString(),
                    Text = x.Message,
                    SongAuthor = song.Author,
                    SongId = song.ID,
                    SongName = song.Name
                };
            }).ToList();
            return Ok(messages);
        }

        [Authorize]
        [HttpPost("{nickname}/messages")]
        public async Task<IActionResult> PostMessage(string nickname)
        {
            //Добавление нового плейлиста, возврат всех плейлистов
            int fromID = DBWork.Instance.GetUser(nickname).Value.ID;
            string from = nickname;
            string to = Request.Form.FirstOrDefault(p => p.Key == "to").Value;
            int toID = DBWork.Instance.GetUser(nickname).Value.ID;
            int songId = int.Parse(Request.Form.FirstOrDefault(p => p.Key == "id").Value);
            string text = Request.Form.FirstOrDefault(p => p.Key == "text").Value;

            DBWork.Instance.CreateMessage(fromID, toID, new List<int>{ songId }, new List<int>(), text );

            return Ok();
        }
    }
}