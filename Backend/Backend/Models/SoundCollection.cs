﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Models
{
    public class SoundCollection
    {
        public int SoundCollectionId { get; set; }
        public string Name { get; set; }
        public bool IsOfficial { get; set; }
    }
}
