﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Models
{
    public class Log
    {
        public int UserId { get; set; }
        public DateTime Time { get; set; }
        public string Message { get; set; }
    }
}
