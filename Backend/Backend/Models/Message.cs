﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Models
{
    public class Message
    {
        public string From { get; set; }
        public string To { get; set; }
        public string Time { get; set; }
        public string Text { get; set; }
        public int SongId { get; set; }
        public string SongName { get; set; }
        public string SongAuthor { get; set; }
    }
}
