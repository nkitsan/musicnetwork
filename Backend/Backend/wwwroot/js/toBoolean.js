﻿document.getElementById("isOfficial").addEventListener("change", saveValue);
const hiddenField = document.getElementById("is_official");
hiddenField.value = false;

function saveValue(e) {
    e.preventDefault();
    hiddenField.value = this.checked;
}