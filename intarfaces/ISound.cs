﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    interface ISound<T>
    {
        List<T> GetSounds(int userId, int playlistId);
        List<T> GetSounds(string soundNamePart);
        void DeleteSound(int userId, int soundId);
    }
}
