﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    interface IMessage<T>
    {
        List<T> GetMessages(int userId);
        void CreateMessage(int userId, string reciverNickname, string content, List<FileStream> files);
    }
}
