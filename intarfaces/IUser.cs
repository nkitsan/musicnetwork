﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    interface IUser<T>
    {
        List<T> GetUserInfo(int userId);
        List<T> GetUsers(string nicknamePart);
        void CreateUser(string nickname, string password, File userpic);
    }
}
