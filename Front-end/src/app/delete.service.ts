import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DeleteService {

  private urlSong = 'http://localhost:57620/songs/';
  private urlPlaylist = 'http://localhost:57620/playlists/';

  constructor(private  http: HttpClient) {

  }

  deleteSong(id: number): Observable<boolean> {
    this.http.delete(this.urlSong + id);
    return of(true);
  }

  deletePlaylist(id: number): Observable<boolean> {
    this.http.delete(this.urlPlaylist + id);
    return of(true);
  }
}
