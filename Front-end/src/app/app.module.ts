import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatNativeDateModule} from '@angular/material';
import {BrowserModule} from '@angular/platform-browser';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {DemoMaterialModule} from './material-module';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { UserPreviewComponent } from './user-page/user-preview/user-preview.component';
import { UserPageComponent } from './user-page/user-page.component';
import { UserPageContentComponent } from './user-page/user-page-content/user-page-content.component';
import { UserTrackComponent } from './user-page/user-page-content/user-track/user-track.component';
import { UserPlaylistComponent } from './user-page/user-page-content/user-playlist/user-playlist.component';
import { UserMessageComponent } from './user-page/user-page-content/user-message/user-message.component';
import { LogsComponent } from './user-page/user-page-content/logs/logs.component';
import { MessageSendComponent } from './message/message-send/message-send.component';
import { MessageViewComponent } from './message/message-view/message-view.component';
import { SearchComponent } from './search/search.component';
import { AppRoutingModule } from './app-routing.module';



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    UserPreviewComponent,
    UserPageComponent,
    UserPageContentComponent,
    UserTrackComponent,
    UserPlaylistComponent,
    UserMessageComponent,
    LogsComponent,
    MessageSendComponent,
    MessageViewComponent,
    SearchComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    DemoMaterialModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

platformBrowserDynamic().bootstrapModule(AppModule);
