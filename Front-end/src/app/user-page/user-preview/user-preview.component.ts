import {Component, Input, OnInit} from '@angular/core';
import {User} from '../../../models/User';
import {UserService} from '../../user.service';
import {FileUploadService} from '../../file-upload.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-user-preview',
  templateUrl: './user-preview.component.html',
  styleUrls: ['./user-preview.component.css']
})
export class UserPreviewComponent implements OnInit {

  identityUser: User;
  selectedUser: string;
  url: 'https://localhost:57620/users/';
  fileToUpload: File = null;

  constructor(private route: ActivatedRoute, private userService: UserService, private fileUploadService: FileUploadService) {

  }

  ngOnInit() {
    this.selectedUser = this.route.snapshot.paramMap.get('username');
    this.userService.getUserIdentity().subscribe(identity => this.identityUser = identity);
    const nickname = this.selectedUser || this.identityUser.nickname;
    this.url += nickname + '/photo';
  }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
    this.uploadFileToActivity();
  }

  uploadFileToActivity() {
    this.fileUploadService.postFileImg(this.fileToUpload, this.identityUser.nickname).subscribe(data => {
    }, error => {
      console.log(error);
    });
  }
}
