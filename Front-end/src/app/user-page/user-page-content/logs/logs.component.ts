import {Component, Input, OnInit} from '@angular/core';
import {Sort} from '@angular/material';
import { Log } from '../../../../models/Log';

@Component({
  selector: 'app-logs',
  templateUrl: './logs.component.html',
  styleUrls: ['./logs.component.css']
})
export class LogsComponent implements OnInit {

  @Input() logs: Log[];

  sortedData: Log[];

  constructor() {
    this.sortedData = this.logs.slice();
  }

  ngOnInit() {

  }

  sortData(sort: Sort) {
    const data = this.logs.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.sortedData = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'time': return this.compare(a.time, b.time, isAsc);
        case 'message': return this.compare(a.message, b.message, isAsc);
        case 'userId': return this.compare(a.userId, b.userId, isAsc);
        default: return 0;
      }
    });
  }

  compare(a, b, isAsc) {
    if (typeof a === 'string' && new Date(a)) {
      return (Date.parse(a) < Date.parse(b) ? -1 : 1) * (isAsc ? 1 : -1);
    }
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

}
