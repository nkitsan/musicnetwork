import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { Song} from '../../../../models/Song';
import {DeleteService} from '../../../delete.service';

@Component({
  selector: 'app-user-track',
  templateUrl: './user-track.component.html',
  styleUrls: ['./user-track.component.css']
})
export class UserTrackComponent implements OnInit {

  @Input() song: Song;
  url = 'http://localhost:57620/songs/';
  @Output() deleteSong: EventEmitter<any> = new EventEmitter();

  constructor(private deleteService: DeleteService) {
    this.url += this.song.id;
  }

  ngOnInit() {
  }

  onClickDelete() {
    this.deleteService.deleteSong(this.song.id);
    this.deleteSong.emit(this.song.id);
  }

}
