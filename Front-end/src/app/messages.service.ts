import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Message } from '../models/Message';

@Injectable({
  providedIn: 'root'
})
export class MessagesService {

  private messagesUrl = 'http://localhost:57620/users/';

  constructor(private http: HttpClient) {
  }

  getMessages(username): Observable<Message[]> {
    return this.http.get<Message[]>(this.messagesUrl + username + '/messages');
  }
}
