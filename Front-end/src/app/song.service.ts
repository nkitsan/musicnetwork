import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Song } from '../models/Song';

@Injectable({
  providedIn: 'root'
})
export class SongService {

  private songsUrl = 'http://localhost:57620/playlists/';

  constructor(private http: HttpClient) {
  }

  getSongs(id): Observable<Song[]> {
    return this.http.get<Song[]>(this.songsUrl + id + '/songs');
  }
}
