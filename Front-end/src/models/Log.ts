export class Log {
  message: string;
  time: string;
  userId: number;
}
